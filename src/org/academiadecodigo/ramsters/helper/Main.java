package org.academiadecodigo.ramsters.helper;

import org.academiadecodigo.ramsters.helper.graphicalmazesolver.MazePrinter;
import org.academiadecodigo.ramsters.helper.graphicalmazesolver.MazeSolver;
import org.academiadecodigo.ramsters.helper.maze.Maze;
import org.academiadecodigo.ramsters.helper.maze.Vector2;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

class Main {
    public static void main(String[] args) throws InterruptedException{

        int mazeSizeX = 50;
        int mazeSizeY = 50;

        Maze maze = new Maze(mazeSizeX, mazeSizeY);
        maze.generateMaze();



        /**
        // terminal maze
        org.academiadecodigo.ramsters.helper.terminalmazesolver.MazePrinter.PrintMaze(maze.getEntrance(), maze.getExit(), mazeSizeX, mazeSizeY);

        String[][] mazeStringArray = org.academiadecodigo.ramsters.helper.terminalmazesolver.MazePrinter.InitializeMazeStringArray(maze.getEntrance(), maze.getExit(), mazeSizeX, mazeSizeY);
        org.academiadecodigo.ramsters.helper.terminalmazesolver.MazePrinter.PrintMazeStringArray(mazeStringArray, maze.getEntrance(), maze.getExit());

        org.academiadecodigo.ramsters.helper.terminalmazesolver.MazeSolver mazeSolver = new org.academiadecodigo.ramsters.helper.terminalmazesolver.MazeSolver(mazeStringArray, maze.getEntrance(), maze.getExit());
        mazeSolver.start(maze.getLongestPath());
        // --------------
        */

        // graphical maze
        MazePrinter mazePrinter = new MazePrinter(20, mazeSizeX, mazeSizeY);
        mazePrinter.init();

        mazePrinter.printMaze(maze.getEntrance(), 1);

        MazeSolver mazeSolver2 = new MazeSolver(mazePrinter, maze.getLongestPath(), 5);
        mazeSolver2.solveMaze(maze.getEntrance(), maze.getExit().getCoordinates());
        // --------------
    }
}








