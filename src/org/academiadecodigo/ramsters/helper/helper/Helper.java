package org.academiadecodigo.ramsters.helper.helper;

import java.util.Random;

public class Helper {

    /**
     * get a random int between two inclusive values
     * @param minRange inclusive
     * @param maxRange inclusive
     * @return
     */
    public static int getRandomInt(int minRange, int maxRange){
        Random random = new Random();

        // +1 because they are both inclusive
        int difference = maxRange - minRange + 1;
        int numberInInterval = random.nextInt(difference) + minRange;

        return numberInInterval;
    }
}
