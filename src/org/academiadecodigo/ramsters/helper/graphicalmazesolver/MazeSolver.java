package org.academiadecodigo.ramsters.helper.graphicalmazesolver;

import org.academiadecodigo.ramsters.helper.maze.Vector2;
import org.academiadecodigo.ramsters.helper.maze.tree.Node;
import org.academiadecodigo.simplegraphics.graphics.Color;

import java.util.Stack;

public class MazeSolver {

    private MazePrinter mazePrinter;

    private Stack<Node> correctPath;

    private int delay;

    public MazeSolver(MazePrinter mazePrinter, Stack<Node> correctPath, int delay){
        this.mazePrinter = mazePrinter;
        this.correctPath = correctPath;
        this.delay = delay;
    }

    // depth first search
    public Node solveMaze(Node root , Vector2 coordinatesToFind) throws InterruptedException{

        Thread.sleep(delay);

        boolean isInCorrectPath = false;

        if(correctPath.peek() == root){

            // if we are on the right track, print in green
            mazePrinter.printSquare(root.getCoordinates(), Color.GREEN);

            // if this isn't root
            if(root.getParent() != null){
                mazePrinter.printConnection(root.getParent().getCoordinates(), root.getCoordinates(), Color.GREEN);
            }

            correctPath.pop();

            // our exit will be in blue
            if(correctPath.size() == 0){
                mazePrinter.printSquare(root.getCoordinates(), Color.BLUE);
            }

            isInCorrectPath = true;
        }

        // just a graphical demonstration of when we go the wrong way
        if(!isInCorrectPath){
            mazePrinter.printSquare(root.getCoordinates(), Color.RED);
            mazePrinter.printConnection(root.getParent().getCoordinates(), root.getCoordinates(), Color.RED);
        }

        if (root.getCoordinates().equals(coordinatesToFind)) {
            return root;
        }

        for (Node child : root.getChildren()) {

            Node correctNode = solveMaze(child, coordinatesToFind);

            if (correctNode != null) {
                return correctNode;
            }
        }

        // print in white when going back to the right path
        mazePrinter.printSquare(root.getCoordinates(), Color.WHITE);
        mazePrinter.printConnection(root.getParent().getCoordinates(), root.getCoordinates(), Color.WHITE);

        return null;
    }

}
