package org.academiadecodigo.ramsters.helper.maze.tree;

import org.academiadecodigo.ramsters.helper.maze.Vector2;

import java.util.List;

public class Node {
    private Vector2 coordinates;
    private Node parent;
    private List<Node> children;

    public Node(Vector2 coordinates, List<Node> children) {
        this.coordinates = coordinates;
        this.children = children;
    }

    public Vector2 getCoordinates(){
        return coordinates;
    }

    public Node getParent(){
        return parent;
    }

    public List<Node> getChildren(){
        return children;
    }

    public void setParent(Node parent){
        this.parent = parent;
    }

    public void setChildren(List<Node> children){
        this.children = children;
    }

    public boolean hasParentOrChildWithCoordinates(Vector2 coordinates){
        if(parent != null && parent.coordinates.equals(coordinates)){
            return true;
        }

        for(Node child : children){
            if(child.coordinates.equals(coordinates)){
                return  true;
            }
        }

        return false;
    }
}