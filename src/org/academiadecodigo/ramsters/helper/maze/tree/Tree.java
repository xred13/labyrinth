package org.academiadecodigo.ramsters.helper.maze.tree;

import org.academiadecodigo.ramsters.helper.maze.Vector2;

import java.util.ArrayList;
import java.util.Stack;

public class Tree {

    private Node root;

    // assume 0,0 as entrance to the mazeTree
    public Tree() {
        this.root = new Node(new Vector2(0, 0), new ArrayList<Node>());
    }

    public Tree(Node root) {
        this.root = root;
    }

    public Tree(Vector2 startingCoordinates) {
        this.root = new Node(startingCoordinates, null);
    }

    public Node getRoot(){
        return root;
    }

    public static Node depthSearchTreeCoordinates(Vector2 coordinatesToFind, Node root) {

        if (root.getCoordinates().equals(coordinatesToFind)) {
            return root;
        }

        for (Node child : root.getChildren()) {
            Node correctNode = depthSearchTreeCoordinates(coordinatesToFind, child);
            if (correctNode != null) {
                return correctNode;
            }
        }

        return null;
    }

    public static Stack<Node> searchLongestPath(Node root){

        // reached tree leafs
        if(root.getChildren().size() == 0){
            Stack<Node> stack = new Stack<>();
            stack.push(root);
            return stack;
        }

        // if we have more than one child, we need to compare the stacks returned by them (we only want the largest)
        Stack<Node> longestStack = new Stack<>();

        for(int i = 0; i < root.getChildren().size(); i++){
            Stack<Node> stack = searchLongestPath(root.getChildren().get(i));

            if(stack.size() > longestStack.size()){
                longestStack = stack;
            }
        }

        longestStack.push(root);

        return longestStack;
    }
}