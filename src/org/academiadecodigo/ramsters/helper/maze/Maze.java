package org.academiadecodigo.ramsters.helper.maze;

import org.academiadecodigo.ramsters.helper.helper.Helper;
import org.academiadecodigo.ramsters.helper.maze.tree.Node;
import org.academiadecodigo.ramsters.helper.maze.tree.Tree;

import java.util.*;

// we consider it to start at (0,0) and go up to (mazeX-1, mazeY-1)
public class Maze {

    // x and y must be smaller than mazeX and mazeY respectively
    private int mazeSizeX, mazeSizeY;

    // coordinates of our entrance to the mazeTree
    private Vector2 startingCoordinates;

    // the coordinates we are currently looking at when generating the mazeTree
    private Vector2 currentCoordinates;

    private Stack<Node> longestPath;

    private Node entrance;
    private Node exit;

    public Tree mazeTree;

    public Maze(int mazeSizeX, int mazeSizeY) {
        this.mazeSizeX = mazeSizeX;
        this.mazeSizeY = mazeSizeY;
        startingCoordinates = new Vector2(0, 0);
        currentCoordinates = new Vector2(startingCoordinates);
        mazeTree = new Tree();
    }

    public Node getEntrance(){
        return entrance;
    }

    public Node getExit(){
        return exit;
    }

    public Stack<Node> getLongestPath(){
        return (Stack<Node>) longestPath.clone();
    }

    public void generateMaze() {

        // Create a way to go back on previous nodes already created
        Stack<Node> nodeGenerationStack = new Stack<>();

        nodeGenerationStack.push(mazeTree.getRoot());

        while(nodeGenerationStack.size() > 0){
            generateMazePath(nodeGenerationStack);
        }

        longestPath = Tree.searchLongestPath(mazeTree.getRoot());

        entrance = longestPath.lastElement();
        exit = longestPath.firstElement();

    }


    // This first mazeTree path algorithm does not take into consideration stopping unless it has no other place to go
    private void generateMazePath(Stack<Node> nodeGenerationStack) {

        boolean finishedGenerating = false;

        while (!finishedGenerating) {

            List<Direction> acceptableDirections = nextGenerationDirection();

            if (acceptableDirections.size() > 0) {

                Direction nextDirection = acceptableDirections.get(Helper.getRandomInt(0, acceptableDirections.size() - 1));

                currentCoordinates = Vector2.add(currentCoordinates, nextDirection.getUnitVector());

                Node child = new Node(currentCoordinates, new ArrayList<>());

                child.setParent(nodeGenerationStack.peek());
                nodeGenerationStack.peek().getChildren().add(child);

                nodeGenerationStack.push(child);

            } else {
                // The current node can no longer generate new paths
                nodeGenerationStack.pop();
                // If we haven't gone back to the root, reassign currentCoordinates so we can keep looking for new paths to generate
                if(nodeGenerationStack.size() > 0){
                    currentCoordinates = nodeGenerationStack.peek().getCoordinates();
                }
                finishedGenerating = true;
            }
        }
    }

    private boolean canGenerateUp(){
        if(currentCoordinates.getY() < mazeSizeY - 1){

            Vector2 coordinatesUp = Vector2.add(currentCoordinates, new Vector2(0, 1));

            if (Tree.depthSearchTreeCoordinates(coordinatesUp, mazeTree.getRoot()) == null) {
                return true;
            }
        }

        return false;
    }

    private boolean canGenerateDown(){
        if(currentCoordinates.getY() > 0){

            Vector2 coordinatesDown = Vector2.add(currentCoordinates, new Vector2(0, -1));

            if (Tree.depthSearchTreeCoordinates(coordinatesDown, mazeTree.getRoot()) == null) { // there aren't any obstructions going down
                // we can go down
                return true;
            }
        }

        return false;
    }

    private boolean canGenerateLeft(){
        if(currentCoordinates.getX() > 0){

            Vector2 coordinatesToTheLeft = Vector2.add(currentCoordinates, new Vector2(-1, 0));

            // check if have already generated to the left
            if (Tree.depthSearchTreeCoordinates(coordinatesToTheLeft, mazeTree.getRoot()) == null) { // there aren't any obstructions to the left
                // we can go left
                return true;
            }
        }

        return false;
    }

    private boolean canGenerateRight(){
        if(currentCoordinates.getX() < mazeSizeX - 1){

            Vector2 coordinatesToTheRight = Vector2.add(currentCoordinates, new Vector2(1, 0));

            if (Tree.depthSearchTreeCoordinates(coordinatesToTheRight, mazeTree.getRoot()) == null) {
                return true;
            }
        }

        return false;
    }

    // tells us if it is possible to keep generating paths on the mazeTree from the currentCoordinates
    // returns a dictionary with booleans saying if we can go "up", "down", "left" or "right"
    private List<Direction> nextGenerationDirection() {

        // a list with the directions we can use to keep generating our maze path
        List<Direction> availableDirections = new ArrayList<>();

        if(canGenerateUp()){
            availableDirections.add(Direction.UP);
        }

        if(canGenerateDown()){
            availableDirections.add(Direction.DOWN);
        }

        if(canGenerateLeft()){
            availableDirections.add(Direction.LEFT);
        }

        if(canGenerateRight()){
            availableDirections.add(Direction.RIGHT);
        }

        return availableDirections;
    }
}