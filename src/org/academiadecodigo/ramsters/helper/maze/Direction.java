package org.academiadecodigo.ramsters.helper.maze;

public enum Direction {
    UP(new Vector2(0, 1)),
    DOWN(new Vector2(0, -1)),
    LEFT(new Vector2(-1, 0)),
    RIGHT(new Vector2(1, 0));

    private Vector2 unitVector;

    Direction(Vector2 unitVector){
        this.unitVector = unitVector;
    }

    public Vector2 getUnitVector(){
        return unitVector;
    }
}
