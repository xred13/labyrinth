package org.academiadecodigo.ramsters.helper.maze;

public class Vector2 {

    private int x, y;

    public Vector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector2(Vector2 vector){
        x = vector.x;
        y = vector.y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public boolean equals(Vector2 vectorToCompare){
        return vectorToCompare.x == this.x && vectorToCompare.y == this.y;
    }

    public void add(Vector2 vector){
        x += vector.x;
        y += vector.y;
    }

    public static Vector2 add(Vector2 vector1, Vector2 vector2) {
        return new Vector2(vector1.x + vector2.x, vector1.y + vector2.y);
    }

    public void printVectorCoordinates(){
        System.out.printf("\nVector X: %d Y: %d\n", x, y);
    }
}