package org.academiadecodigo.ramsters.helper.terminalmazesolver;

import org.academiadecodigo.ramsters.helper.helper.Helper;
import org.academiadecodigo.ramsters.helper.maze.Vector2;
import org.academiadecodigo.ramsters.helper.maze.tree.Node;
import org.academiadecodigo.ramsters.helper.maze.tree.Tree;

public class MazePrinter {

    private static char entranceSymbol = 'E';
    private static char exitSymbol = 'O';

    public static void PrintMaze(Node root, Node exit, int mazeSizeX, int mazeSizeY){
        String[][] mazeStringArray = InitializeMazeStringArray(root, exit, mazeSizeX, mazeSizeY);

        PrintMazeStringArray(mazeStringArray, root, exit);
    }

    // root is our entrance
    public static String[][] InitializeMazeStringArray(Node root, Node exit, int mazeSizeX, int mazeSizeY){
        String[][] mazeStringArray = new String[mazeSizeX][mazeSizeY];

        for(int x = 0; x < mazeStringArray.length; x++){
            for(int y = 0; y < mazeStringArray[0].length; y++){

                Node found = Tree.depthSearchTreeCoordinates(new Vector2(x, y), root);

                // we are building the mazeTree from left to right, so we need to check if this node's child or parent is on his right or bottom coordinates
                // if they are, then we build a pathway to them
                // otherwise, we have to create a wall between them
                Vector2 rightSideCoordinates = Vector2.add(found.getCoordinates(), new Vector2(1, 0));
                Vector2 bottomSideCoordinates = Vector2.add(found.getCoordinates(), new Vector2(0, -1));

                // first check on the right
                boolean hasParentOrChildToTheRight = found.hasParentOrChildWithCoordinates(rightSideCoordinates);

                // then check on the bottom
                boolean hasParentOrChildToTheBottom = found.hasParentOrChildWithCoordinates(bottomSideCoordinates);

                String mazeSquareString = "";
                if(hasParentOrChildToTheBottom){
                    if(found.getCoordinates().equals(exit.getCoordinates())){
                        mazeSquareString += exitSymbol;
                    }
                    else{
                        mazeSquareString += " ";
                    }
                }
                else{
                    if(found.getCoordinates().equals(root.getCoordinates())){
                        mazeSquareString += entranceSymbol;
                    }
                    else if(y == 0){
                        if(found.getCoordinates().equals(exit.getCoordinates())){
                            mazeSquareString += exitSymbol;
                        }
                        else{
                            mazeSquareString += " ";
                        }
                    }
                    else{
                        if(found.getCoordinates().equals(exit.getCoordinates())){
                            mazeSquareString += "\033[0;4m" + exitSymbol + "\033[0;0m";
                        }
                        else{
                            mazeSquareString += "_";
                        }
                    }
                }

                if(hasParentOrChildToTheRight){
                    if(!hasParentOrChildToTheBottom && y != 0){
                        mazeSquareString += "_";
                    }
                    else{
                        mazeSquareString += " ";
                    }
                }
                else{

                    mazeSquareString += "|";
                }

                mazeStringArray[x][y] = mazeSquareString;
            }
        }

        return mazeStringArray;
    }

    public static void PrintMazeStringArray(String[][] mazeStringArray, Node entrance, Node exit){

        int randomRow = Helper.getRandomInt(0, mazeStringArray[0].length);

        System.out.println(randomRow);
        // print the top border of the mazeTree
        for(int x = 0; x < 2 + mazeStringArray.length * 2 - 1; x++){
            System.out.print("_");
        }
        System.out.print("\n");

        // we will print the array from top to bottom, left to right
        for(int y = mazeStringArray[0].length - 1; y >= 0; y--){
            // mazeTree left border
            System.out.print("|");
            for(int x = 0; x < mazeStringArray.length; x++){
                System.out.print(mazeStringArray[x][y]);
            }
            if(y == randomRow){
                System.out.print("           O <- I'm out \\o/");
            }
            System.out.print("\n");
        }

        // print the bottom border of the mazeTree
        for(int x = 0; x < 2 + mazeStringArray.length * 2 - 1; x++){
            System.out.print("\u00AF");
        }
    }
}
